
from keras.utils import to_categorical

import time
from A3C import Agent
from .PongBasic import PongEnv
from timeit import default_timer as timer
import ray

#@ray.remote
class ExperienceCollector(object):
    def __init__(self, consecutive_frames, render = False):
        self.env = PongEnv(consecutive_frames, render=render)
        self.state_dim = self.env.get_state_size()
        self.action_dim = self.env.get_action_size()
        self.agent = Agent.A3CNN(self.state_dim, self.action_dim)

        self.old_state = self.env.reset()
        self.episode_reward = 0

    def updateWeights(self, weights):
        self.agent.model.set_weights(weights)

    def runEnv(self, gatherInterval, thread_delay=0.0):
        actions, states, rewards, dones = [], [], [], []

        step = 0
        done = False
        while not done and step < gatherInterval:

            a = self.agent.policy_action(self.old_state)
            new_state, r, done, _info = self.env.step(a)
            if thread_delay > 0:
                time.sleep(0.0005)
            step += 1
            actions.append(to_categorical(a, self.action_dim))
            rewards.append(r)
            dones.append(done)
            states.append(self.old_state)
            self.old_state = new_state
            self.episode_reward += r
            if done:
                print("Reward %d" % (int(self.episode_reward)))
                #    tf.summary.scalar('learning rate', data=episode_reward, step=episode)
                self.episode_reward = 0
                self.old_state = self.env.reset()

        return actions, states, rewards, dones
