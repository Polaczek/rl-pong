import gym
from gym import wrappers
import numpy as np
import cv2
from gym import spaces
import threading

class NoopResetEnv(gym.Wrapper):
    def __init__(self, env, noop_max=30):
        """Sample initial states by taking random number of no-ops on reset.
        No-op is assumed to be action 0.
        """
        gym.Wrapper.__init__(self, env)
        self.noop_max = noop_max
        self.override_num_noops = None
        if isinstance(env.action_space, gym.spaces.MultiBinary):
            self.noop_action = np.zeros(self.env.action_space.n, dtype=np.int64)
        else:
            # used for atari environments
            self.noop_action = 0
            assert env.unwrapped.get_action_meanings()[0] == 'NOOP'

    def _reset(self, **kwargs):
        """ Do no-op action for a number of steps in [1, noop_max]."""
        self.env.reset(**kwargs)
        if self.override_num_noops is not None:
            noops = self.override_num_noops
        else:
            noops = self.unwrapped.np_random.randint(1, self.noop_max + 1) #pylint: disable=E1101
        assert noops > 0
        obs = None
        for _ in range(noops):
            obs, _, done, _ = self.env.step(self.noop_action)
            if done:
                obs = self.env.reset(**kwargs)
        return obs

class EpisodicLifeEnv(gym.Wrapper):
    def __init__(self, env):
        """Make end-of-life == end-of-episode, but only reset on true game over.
        Done by DeepMind for the DQN and co. since it helps value estimation.
        """
        gym.Wrapper.__init__(self, env)
        self.lives = 0
        self.was_real_done  = True

    def _step(self, action):
        obs, reward, done, info = self.env.step(action)
        self.was_real_done = done
        # check current lives, make loss of life terminal,
        # then update lives to handle bonus lives
        lives = self.env.unwrapped.ale.lives()
        if lives < self.lives and lives > 0:
            # for Qbert somtimes we stay in lives == 0 condtion for a few frames
            # so its important to keep lives > 0, so that we only reset once
            # the environment advertises done.
            done = True
        self.lives = lives
        return obs, reward, done, info

    def _reset(self, **kwargs):
        """Reset only when lives are exhausted.
        This way all states are still reachable even though lives are episodic,
        and the learner need not know about any of this behind-the-scenes.
        """
        if self.was_real_done:
            obs = self.env.reset(**kwargs)
        else:
            # no-op step to advance from terminal/lost life state
            obs, _, _, _ = self.env.step(0)
        self.lives = self.env.unwrapped.ale.lives()
        return obs
class WarpFrame(gym.ObservationWrapper):
    def __init__(self, env, x, y):
        """Warp frames to 84x64"""
        gym.ObservationWrapper.__init__(self, env)
        self.width = x
        self.height = y
        self.observation_space = spaces.Box(low=0, high=1, shape=(self.height, self.width, 1))

    def observation(self, observation):
        I = observation[35:195, 16:144]  # crop
        I = I[::2, ::2, 0]  # downsample by factor of 2
        I[I == 144] = 0  # erase background (background type 1)
        I[I == 109] = 0  # erase background (background type 2)
        I[I != 0] = 1  # everything else (paddles, ball) just set to 1

        return I[:, :, None].astype(float)

    def render(self, mode='human'):
        if mode == 'rgb_array':
            return self.observation(super().render(mode=mode))
        else:
            return super().render(mode=mode)


class PongEnv(object):
    def __init__(self, consecutive_frames, render):
        env = gym.make('Pong-v0')
        env = NoopResetEnv(env, noop_max=30)
        env = WarpFrame(env, 80 , 64) #80x64x1 is the result of downscaling cropped 2d image
        if consecutive_frames > 1:
            env = wrappers.FrameStack(env, consecutive_frames)
        if render and consecutive_frames not in [1, 3]:
            raise IndexError("Cannot render stack frame environments different than 3 and 1 stack")
        self.env = env

        self.action_space = self.env.action_space
        self.observation_space = self.env.observation_space
        self.consecutive_frames = consecutive_frames
        self.do_render = render

        self.wins = 0

    def seed(self, seed):
        self.env.seed(seed)

    def reset(self):
        observation = self.env.reset()
        if self.consecutive_frames > 1:
            x = np.array(observation)
            x = np.moveaxis(x.squeeze(), 0, -1)
        self.wins = 0
        return x

    def step(self,action):
        if not self.env.action_space.contains(action):
            raise ValueError('Invalid action!!')

        observation, reward, done, info = self.env.step(action)
        if self.consecutive_frames > 1:
            x = np.array(observation)
            x = np.moveaxis(x.squeeze(), 0, -1)

        if reward == 1:
            self.wins += 1
        if done and False:
            print("Episode done, reward %d" % self.wins)
        return x, reward, done, info

    def get_action_size(self):
        return self.env.action_space.n

    def get_state_size(self):
        return 80, 64, self.consecutive_frames

    def get_random_action(self):
        return self.action_space.sample()

    def render(self):
        #obs = self.env.render(mode='rgb_array')
        obs = self.env._get_observation()
        if self.consecutive_frames > 1:
            obs = np.array(obs)
            obs = np.moveaxis(obs.squeeze(), 0, -1)
        upscaled = np.kron(obs, np.ones((3,3,1)))
        cv2.imshow('pong_%d' % threading.get_ident(), upscaled * 255)
        cv2.waitKey(1)
        return obs

