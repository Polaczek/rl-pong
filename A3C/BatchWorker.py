import numpy as np
from keras.utils import to_categorical

from .PongBasic import PongEnv
from timeit import default_timer as timer
import multiprocessing

class BatchEnvWorker(multiprocessing.Process):
    def __init__(self, workerId, actionPipe, stateQueue, trainingQueue, config):
        super(BatchEnvWorker, self).__init__()
        self.workerId = workerId
        self.actionPipe = actionPipe
        self.stateQueue = stateQueue
        self.trainingQueue = trainingQueue
        self.config = config

    def run(self):
        while (True):
            try:
                no_envs = self.config["no_envs"]
                envs = [PongEnv(consecutive_frames=self.config["consecutive_frames"], render=False) for i in range(no_envs)]
                old_states = [env.reset() for env in envs]
                done = False
                no_actions = envs[0].get_action_size()

                actions, states, rewards, dones = [[] for i in range(no_envs)], \
                                                  [[] for i in range(no_envs)], \
                                                  [[] for i in range(no_envs)],\
                                                  [[] for i in range(no_envs)]
                steps, episode_reward = [0 for i in range(no_envs)], [0 for i in range(no_envs)]
                episode = 0

                inf_time, step_time, train_time = 0, 0, 0
                while True:
                    old_states_vec = np.array(old_states).squeeze()
                    #Push state to agent
                    self.stateQueue.put((self.workerId, old_states_vec))
                    a = self.actionPipe.recv()

                    # Handle env stepping
                    data_batch = [env.step(int(a[idx])) for idx, env in enumerate(envs)]
                    new_states = [data[0] for data in data_batch]
                    r = [data[1] for data in data_batch]
                    done = [data[2] for data in data_batch]
                    # info = [data[3] for data in data_batch]
                    for idx in range(no_envs):
                        actions[idx].append(to_categorical(a[idx], no_actions))
                        episode_reward[idx] += r[idx]
                        rewards[idx].append(r[idx])
                        states[idx].append(old_states[idx])
                        dones[idx].append(done[idx])
                        steps[idx] += 1

                    old_states = new_states

                    # Evaluate episodes
                    for idx in range(no_envs):
                        if steps[idx] > self.config["training_interval"] or abs(r[idx]) > 0:
                            self.trainingQueue.put((np.array(actions[idx])[:, 0, :], states[idx], rewards[idx],
                                               dones[idx]))

                            actions[idx], states[idx], rewards[idx], dones[idx] = [], [], [], [] # reset accumulator
                            steps[idx] = 0
                            if done[idx]:
                                print("Episode done, reward:%d" % episode_reward[idx])
                                episode_reward[idx] = 0
                                episode += 1
                                old_states[idx] = envs[idx].reset()


            except (KeyboardInterrupt, SystemExit):
                break

def inferenceThread(agent, stateQueue, actionPipes):
    no_workers = len(actionPipes)
    try:
        while True:
            worker_idx, state = stateQueue.get()  # Fetch states
            actionPipes[worker_idx].send(agent.agent.policy_action(state))  # Push actions
    except (KeyboardInterrupt, SystemExit):
        pass
