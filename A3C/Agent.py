
import numpy as np
import sys
import tensorflow as tf
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense, Flatten, Reshape, Conv2D, MaxPooling2D, concatenate
from tensorflow.keras.optimizers import Adam, RMSprop
import tensorflow.keras.backend as K
import os

from datetime import datetime
# Sets up a timestamped log directory.
logdir = "logs/train_data/" + datetime.now().strftime("%Y%m%d-%H%M%S")
# Creates a file writer for the log directory.
file_writer = tf.summary.create_file_writer(logdir)
file_writer.set_as_default()



def conv_layer(d, k):
    """ Returns a 2D Conv layer, with and ReLU activation
    """
    return Conv2D(d, k, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')
def conv_block(inp, d=3, pool_size=(2, 2), k=3):
    """ Returns a 2D Conv block, with a convolutional layer, max-pooling
    """
    print(inp.shape)
    conv = conv_layer(d, k)(inp)
    return MaxPooling2D(pool_size=pool_size)(conv)





def loss_custom(true_val, pred_val):
    LOSS_ENTROPY = 0.01
    LOSS_V = 0.5
    p, v = tf.split(pred_val, [6 ,1], axis=1)
    a_t, r_t = tf.split(true_val, [6, 1], axis=1)
    logp = tf.math.log(tf.reduce_sum(p*a_t, axis=1, keepdims=True) + 1e-10)
    advantage = r_t - v
    loss_policy = -logp * tf.stop_gradient(advantage)
    loss_value = LOSS_V * tf.square(advantage)
    entropy = LOSS_ENTROPY * tf.reduce_sum(p * tf.math.log(p + 1e-10), axis=1, keepdims=True)
    loss_total = tf.reduce_mean(loss_policy + loss_value + entropy)
    return loss_total


class A3CNN():
    def __init__(self, inp_dim, out_dim, lr=5e-4, optimizer=False):
        self.name=["A3CSave", "A3C2Save", "A3C3Save"][2]

        inp = Input(batch_shape=(None,) + inp_dim)
        x = conv_block(inp, 16, (2, 2))
        x = conv_block(x, 32, (2, 2))
        x = Flatten()(x)
        shared_out = Dense(256, activation='relu')(x)
        action_output = Dense(out_dim, activation='softmax')(shared_out)
        value_output = Dense(1, activation='linear')(shared_out)

        output = concatenate([action_output, value_output])
        self.model = Model(inputs=inp, outputs=output)
        self.inp_dim = inp_dim
        self.out_dim = out_dim
        self.lr = lr
        if optimizer:
            self.opt = Adam(lr=self.lr)
            self.model.compile(loss=loss_custom, optimizer=self.opt)
        else:
            self.opt = None

        self.train_loss = []
        self.loss_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)#tf.keras.callbacks.callbacks.BaseLogger(stateful_metrics=None)#

    def predict(self, inp):
        action_value = self.model.predict(self.reshape(inp))
        action, value = np.hsplit(action_value, [self.out_dim])
        return [action, value]

    def policy_action(self, s):
        #random choice based on probability returned by A3C
        action, value = self.predict(s)
        if(s.shape[0] == 1):
            return np.random.choice(np.arange(self.out_dim), 1, p=action.ravel())[0]
        else:
            actions = np.zeros((s.shape[0], 1), dtype=int)
            for idx in range(s.shape[0]):
                actions[idx] = np.random.choice(np.arange(self.out_dim), 1, p=action[idx, :].ravel())[0]
            return actions

    def reshape(self, x):
        #Handles non-batch data
        if len(x.shape) < 4 and len(self.inp_dim) > 2: return np.expand_dims(x, axis=0)
        return x

    def train(self, inp, actions, discounted_rewards):
        if self.opt is None:
            raise AttributeError("Called model without optimizer")
        #y_pred = self.model.predict(self.reshape(inp))
        discounted_rewards = discounted_rewards[:,None]
        y_true = tf.convert_to_tensor(np.concatenate([actions, discounted_rewards], axis=1))
        train_history = self.model.fit(inp, y_true, verbose=0)#callbacks=[self.loss_callback]
        self.train_loss.append(train_history.history['loss'][0])


    def getPath(self):
        return os.path.join('network_weights', self.name+ '_LR_{}.h5'.format(self.lr))

    def getPathOld(self):
        return self.name+ '_LR_{}'.format(self.lr)

    def save(self):
        path = self.getPath()
        if not os.path.exists('network_weights'):
            os.mkdir('network_weights')
        self.model.save_weights(path)

    def load_weights(self):
        path = self.getPath()
        if not os.path.exists('network_weights'):
            os.mkdir('network_weights')
        try:
            self.model.load_weights(path)
        except Exception as e:
            print("Could not load weights from %s" % path)
            print(e)
