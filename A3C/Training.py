from threading import Thread, Lock
import threading
import numpy as np
from keras.utils import to_categorical
import cv2
import tensorflow as tf
import time
from .PongBasic import PongEnv
from timeit import default_timer as timer
import multiprocessing


episode = 0
lock = Lock()

def training_thread(agent, Nmax, env, action_dim, f, ckpt_freq=25, sleeptime = 0, render=False):
    global episode
    while episode < Nmax:
        timex, cumul_reward, done = 0, 0, False
        old_state = env.reset()
        actions, states, rewards = [], [], []

        episode_reward = 0
        timeStat0 = 0
        timeStat1 = 0
        timeStat2 = 0
        while not done and episode < Nmax:
            if render and (threading.current_thread() is threading.main_thread()):
                with lock:
                    env.render()
            t0 = time.time()
            a = agent.agent.policy_action(old_state)
            t1 = time.time()
            new_state, r, done, _info = env.step(a)
            t2 = time.time()
            timeStat0 += t1-t0
            timeStat1 += t2-t1
            if sleeptime > 0:
                time.sleep(sleeptime)
            actions.append(to_categorical(a, action_dim))
            rewards.append(r)
            states.append(old_state)
            old_state = new_state
            cumul_reward += r
            timex += 1
            episode_reward +=r
            if done:
                with lock:
                    print("Reward %d, episode %d/%d" % (int(episode_reward), episode, Nmax))
                    tf.summary.scalar('learning rate', data=episode_reward, step=episode)

                    episode_reward = 0

                    if (episode < Nmax):
                        episode += 1
                    if (episode > 0 and episode % ckpt_freq == 0):
                        agent.save_weights(episode)
            if(timex%f==0 or done):
                t3 = time.time()
                lock.acquire()
                agent.train_models(actions, states, rewards, done)
                t4 = time.time()
                print("Inference %f, Sim time %f, training %f" % (timeStat0, timeStat1, t4-t3))
                timeStat0 = 0
                timeStat1 = 0
                lock.release()
                actions, states, rewards = [], [], []


def training_batch(agent, envs, ckpt_freq, f, episodes):
    no_envs = len(envs)
    old_states = [env.reset() for env in envs]
    done = False

    actions, states, rewards = [[] for i in range(no_envs)],[[] for i in range(no_envs)],[[] for i in range(no_envs)]
    steps, episode_reward = [0 for i in range(no_envs)], [0 for i in range(no_envs)]
    episode = 0

    inf_time, step_time, train_time = 0, 0, 0
    while not done and episode < episodes:
        t0 = timer()
        old_states_vec = np.array(old_states).squeeze()
        a = agent.agent.policy_action(old_states_vec)
        t1 = timer()
        inf_time += t1-t0
        #Handle env stepping
        data_batch = [env.step(int(a[idx])) for idx, env in enumerate(envs)]
        new_states = [data[0] for data in data_batch]
        r = [data[1] for data in data_batch]
        dones = [data[2] for data in data_batch]
        #info = [data[3] for data in data_batch]
        for idx in range(no_envs):
            actions[idx].append(to_categorical(a[idx], agent.agent.out_dim))
            episode_reward[idx] += r[idx]
            rewards[idx].append(r[idx])
            states[idx].append(old_states[idx])
            steps[idx] += 1
        old_states = new_states
        t2=timer()
        step_time += t2-t1
        #Evaluate episodes
        trained = False
        for idx in range(no_envs):
            if steps[idx] ==f or dones[idx]:
                t3 = timer()
                agent.train_models(np.array(actions[idx])[:,0,:], states[idx], rewards[idx], dones[idx]) #TODO dones is not calc correctly
                actions[idx], states[idx], rewards[idx] = [], [], [] # reset accumulator
                steps[idx] = 0
                t4=timer()
                train_time += t4-t3
                trained = True
                if dones[idx]:
                    print("Episode done, reward:%d" % episode_reward[idx])
                    episode_reward[idx] = 0
                    episode += 1
                    if (episode > 0 and episode % ckpt_freq == 0):
                        agent.save_weights(episode)
                    old_states[idx] = envs[idx].reset()
        if trained:
            print("Inference %f, step %f, train %f" % (inf_time, step_time, train_time))
            inf_time, step_time, train_time = 0, 0, 0
        if episode > episodes:
            break


class BatchEnvWorker(multiprocessing.Process):
    def __init__(self, workerId, actionPipe, stateQueue, trainingQueue, config):
        super(BatchEnvWorker, self).__init__()
        self.workerId = workerId
        self.actionPipe = actionPipe
        self.stateQueue = stateQueue
        self.trainingQueue = trainingQueue
        self.config = config

    def run(self):
        while (True):
            try:
                no_envs = self.config["no_envs"]
                envs = [PongEnv(consecutive_frames=self.config["consecutive_frames"], render=False) for i in range(no_envs)]
                old_states = [env.reset() for env in envs]
                done = False
                no_actions = envs[0].get_action_size()

                actions, states, rewards, dones = [[] for i in range(no_envs)], \
                                                  [[] for i in range(no_envs)], \
                                                  [[] for i in range(no_envs)],\
                                                  [[] for i in range(no_envs)]
                steps, episode_reward = [0 for i in range(no_envs)], [0 for i in range(no_envs)]
                episode = 0

                inf_time, step_time, train_time = 0, 0, 0
                while True:
                    old_states_vec = np.array(old_states).squeeze()
                    #Push state to agent
                    self.stateQueue.put((self.workerId, old_states_vec))
                    a = self.actionPipe.recv()

                    # Handle env stepping
                    data_batch = [env.step(int(a[idx])) for idx, env in enumerate(envs)]
                    new_states = [data[0] for data in data_batch]
                    r = [data[1] for data in data_batch]
                    done = [data[2] for data in data_batch]
                    # info = [data[3] for data in data_batch]
                    for idx in range(no_envs):
                        actions[idx].append(to_categorical(a[idx], no_actions))
                        episode_reward[idx] += r[idx]
                        rewards[idx].append(r[idx])
                        states[idx].append(old_states[idx])
                        dones.append(done[idx])
                        steps[idx] += 1

                    old_states = new_states

                    # Evaluate episodes
                    for idx in range(no_envs):
                        if steps[idx] == self.config["training_interval"] or dones[idx]:
                            self.trainingQueue.put((np.array(actions[idx])[:, 0, :], states[idx], rewards[idx],
                                               dones[idx]))

                            actions[idx], states[idx], rewards[idx] = [], [], []  # reset accumulator
                            steps[idx] = 0
                            if dones[idx]:
                                print("Episode done, reward:%d" % episode_reward[idx])
                                episode_reward[idx] = 0
                                episode += 1
                                old_states[idx] = envs[idx].reset()


            except (KeyboardInterrupt, SystemExit):
                break