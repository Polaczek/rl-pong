

import time
import threading
import numpy as np



from .Agent import A3CNN
from .PongBasic import PongEnv
#import ray
#ray.init()

from .Training import  training_thread, training_batch
from .BatchWorker import BatchEnvWorker, inferenceThread
#from .RayWorkers import ExperienceCollector

import multiprocessing
import queue

class A3C:
    def __init__(self, act_dim, env_dim, k, gamma=0.99, lr = 5e-4):
        self.act_dim = act_dim
        self.env_dim = env_dim
        self.gamma = gamma
        self.lr = lr
        self.agent = A3CNN(self.env_dim, self.act_dim, self.lr, optimizer=True)
        self.loss_samples = 100


    def discount(self, r, done, s):
        discounted_r, cumul_r = np.zeros_like(r), 0
        for t in reversed(range(0, len(r))):
            cumul_r = r[t] + cumul_r * self.gamma
            discounted_r[t] = cumul_r
        return  discounted_r

    def save_weights(self, episode):
        print("Checkpoint weights, episode %d" % episode)
        self.agent.save()

    def train_models(self, actions, states, rewards, dones):
        states = np.array(states)
        actions = np.array(actions)
        discounted_rewards = self.discount(rewards, dones, states[-1])

        self.agent.train(states, actions, discounted_rewards)
        if len(self.agent.train_loss) >= self.loss_samples and len(self.agent.train_loss) % self.loss_samples == 0 and False:
            print("Average loss from last %d train calls is %.2E" % (self.loss_samples,
                                                                     sum(self.agent.train_loss[-10:]) / self.loss_samples))


    def evaluate(self,env, consecutive_frames):
        env = PongEnv(consecutive_frames, render=True)
        self.agent.load_weights()

        obs = env.reset()
        while True:
            env.render()
            action = self.agent.policy_action(obs[None,:])
            obs, r, done, _info = env.step(action)


    def train(self, consecutive_frames, render, n_threads):
        self.agent.load_weights()

        nb_episodes = 200
        training_interval = 128
        ckpt_freq = 25

        episode = 0
        useRay=False
        if n_threads > 1 and useRay:
            envs = [ExperienceCollector.remote(consecutive_frames) for i in range(n_threads)]
            while(nb_episodes > 0):
                experienceID = [c.runEnv.remote(training_interval) for c in envs]
                experience = ray.get(experienceID)
                #process experience and update weights
                eps_done = 0
                for actions, states, rewards, dones in experience:
                    self.train_models(actions, states, rewards, dones)
                    eps_done += sum(dones)

                for i in range(eps_done):
                    episode += 1
                    nb_episodes -= 1
                    if episode > 0 and episode % ckpt_freq == 0:
                        self.agent.save()
                        print("%d episodes left" % nb_episodes)


                #Update agent weights and repeat
                updatedNN = self.agent.model.get_weights()
                [c.updateWeights.remote(updatedNN) for c in envs]

        elif n_threads > 1:
            '''
            envs = [PongBasic.PongEnv(consecutive_frames, render) for i in range(n_threads)]
            state_dim = envs[0].get_state_size()
            action_dim = envs[0].get_action_size()

            nb_episodes = 200
            training_interval = 128
            ckpt_freq = 25

            threads = [threading.Thread(
                target=training_thread,
                daemon=True,
                args=(self,
                      nb_episodes,
                      envs[i],
                      action_dim,
                      training_interval,
                      ckpt_freq,
                      0.001,
                      render)) for i in range(n_threads)]

            for t in threads:
                t.start()
                time.sleep(0.5)
            try:
                [t.join() for t in threads]
            except KeyboardInterrupt:
                print("Exiting all threads...")
                '''
            n_threads = 2
            actionPipes, childPipes = [], []
            for i in range(n_threads):
                parentPipe, childPipe = multiprocessing.Pipe()
                actionPipes.append(parentPipe)
                childPipes.append(childPipe)
            #statePipes = [multiprocessing.Pipe() for i in range(n_threads)]
            stateQueue = multiprocessing.Queue()
            trainingQueue = multiprocessing.Queue()
            config = {"training_interval":training_interval,
                      "no_envs":32,
                      "consecutive_frames":consecutive_frames}
            envWorkers = [BatchEnvWorker(id, childPipes[id], stateQueue, trainingQueue, config) for id in range(n_threads)]
            inference_thread = threading.Thread(target=inferenceThread,
                daemon=True,
                args=(self, stateQueue, actionPipes))
            try:
                for worker in envWorkers:
                    worker.start()
                episodes_done = 0
                # Spawn inference thread
                inference_thread.start()
                while(episodes_done < nb_episodes):
                    #inference stage

                    '''
                    try:
                        while True:
                            worker_idx, state = stateQueue.get(block=False) #Fetch states
                            actionPipes[worker_idx].send(self.agent.policy_action(state)) #Push actions
                    except queue.Empty:
                        pass
                    '''
                    #training stage

                    try:
                        while True:
                            actions, states, rewards, dones = trainingQueue.get()
                            self.train_models(actions, states, rewards, dones)
                            for i in range(sum(dones)):
                                episodes_done += 1
                                if episodes_done % ckpt_freq == 0:
                                    print("Saving weights, episode %d" % episodes_done)
                                    self.save_weights(episodes_done)
                    except queue.Empty:
                        pass

            except KeyboardInterrupt:
                inference_thread.join()
                for worker in envWorkers:
                    worker.terminate()
                    worker.join()

        else:
            #Batch, single thread training
            envs = [PongEnv(consecutive_frames, render) for i in range(32)]
            training_batch(self, envs, ckpt_freq, training_interval, nb_episodes)
            pass

        self.agent.save()