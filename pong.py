from A3C.PongBasic import PongEnv
from A3C.A3C import A3C


consecutive_frames = 3
n_threads = 2
render = False

evaluate = False
if __name__ == "__main__":
    env = PongEnv(consecutive_frames, False)
    state_dim = env.get_state_size()
    action_dim = env.get_action_size()
    algo = A3C(action_dim, state_dim, consecutive_frames)
    if not evaluate:
        algo.train(consecutive_frames, render, n_threads)
    else:
        algo.evaluate(env, consecutive_frames)